#!/usr/bin/env python

import requests
from random import randint
from json import loads
from os import environ
import tweepy


OWL_IMG = "owl.jpg"
URL = "https://pixabay.com/api/"
TWEET = " ~ One More Owl ~ "

params = {
    "key": environ.get("PIXA_API_KEY"),
    "q": "owl",
    "image_type": "photo",
    "safesearch": "true",
    "order": "latest",
    "per_page": 3,
    "page": randint(0, 160)
}

req = requests.get(url=URL, params=params)
print("HTTP Code: {:d}".format(req.status_code))
print(req.text)

try:
    data = loads(req.text)
except Exception as e:
    print("Error: ", e)
    exit(1)

picture_url = data["hits"][randint(0, 2)]["largeImageURL"]
print("Sweet, I'll post " + picture_url)

with open(OWL_IMG, "wb") as handler:
    handler.write(requests.get(picture_url).content)

# Personal information
app_key = environ.get("APP_KEY")
app_key_secret = environ.get("APP_KEY_SECRET")
token = environ.get("TOKEN")
token_secret = environ.get("TOKEN_SECRET")

# Authentication
auth = tweepy.OAuthHandler(app_key, app_key_secret)
auth.set_access_token(token, token_secret)

# Tweet
api = tweepy.API(auth)
api.update_with_media(OWL_IMG, TWEET)
